/*����, ���������� ������� ���������� � �������*/
#include <stdio.h>
#include <stdlib.h>
#include "compressor.h"


void find_freq(struct SYM * sym, FILE * file)
{
	int i = 0;
	int count = 0;
	int temp = '0';

	for (i = 0; i < 256; i++)
	{
		sym[i].ch = i;
		sym[i].freq = 0;

	}

	while ((temp = fgetc(file)) != EOF)
	{
		count++;
		sym[temp].freq += 1.;

	}

	for (i = 0; i < 256; i++)
	{
		sym[i].freq /= count;

	}
}

int find_count(struct SYM ** sym)
{
	int i, n = 0;
	for (i = 0; i < 256; i++)
	{
		if (sym[i]->freq > 0)
		n++;
	
	}
	return n;
}

void sort_freq(struct SYM *psym[]) //��� ���������� ������� ����������
{
	int i = 0, flag = 1;
	struct SYM * t = NULL;
	while (flag != 0)
	{
		flag = 0;
		for (i = 0; i < 255; i++)
		{
			if (psym[i + 1]->freq > psym[i]->freq)
			{
				flag = 1;
				t = psym[i];
				psym[i] = psym[i + 1];
				psym[i + 1] = t;
			}
		}
	}
}


void print_to_101(int ch, FILE *fp_in, FILE *fp_101, struct SYM * sym, char file_name[])
{
	int i;
	fp_in = fopen(file_name, "rb");
	fp_101 = fopen("temp.101", "wb");
	while ((ch = fgetc(fp_in)) != -1)
	{
		for (i = 0; i < 256; i++)
			if (sym[i].ch == (unsigned char)ch) {
				fputs(sym[i].code, fp_101); // ������� ������ � �����
				break; // ��������� �����
			}
	}
	fclose(fp_in);
	fclose(fp_101);
}