#include <stdio.h>
#include <stdlib.h>
#include "compressor.h"


void compressing() //�������, �������������� ������ �����
{

	int arr[8];//������ 0 � 1
	int i = 0;
	int j = 0;
	union code code1;//�������������� ���������� code1
	int fsize = 0;//����������,� ������� ����� ��������� ������ ����� .101
	int ost = 0;//������� ����� .101
	struct SYM sym[256];
	struct SYM *psym[256];
	FILE * source = NULL;//�������� ����
	FILE * temp = NULL;//������������� ����(0 � 1)
	FILE * compressed = NULL;//������ ����
	struct SYM *root;
	char file_name[30];//����� �������� ��� �����,���������� ��� ������
	int  count = 0;//���������� ���������� ��������
	int ch = 0;// � ��� ���������� �������� ���������� �� �����


	for (i = 0; i < 256; i++) //��������� ������ �������� � �������� ���������� �� ���������,������ ������������� ������ ��������
	{
		psym[i] = &sym[i];
		sym[i].code[0] = '\0';
		sym[i].left = sym[i].right = 0;
	}


	printf("Please, enter name of file for compressing:\n");
	scanf("%s", &file_name);
	source = fopen(file_name, "rb");

	if (file_name == NULL)
	{
		perror("Can't open file!\n");
		exit(1);
	}

	find_freq(sym, source);

	sort_freq(psym);

	count = find_count(psym);

	root = buildTree(psym, count);

	makeCodes(root);

	rewind(source);

	print_to_101(ch, source, temp, sym, file_name); //�������� �������������� ������� � ���� .101

	fclose(source);

	temp = fopen("temp.101", "rb");

	compressed = fopen("compressed.txt", "wb");//��������� ���� ��� ������ ������� �����

	while ((ch = fgetc(temp)) != EOF)//������� ���������� �������� � �������� �����
		fsize++;
	ost = fsize % 8;//������� �������, ���������� �������� �� ������� 8 (�����)

	//��������� ��������� ������� ����� ����� ���� ������
	fwrite("compressing", sizeof(char), 12, compressed);//�������� �������
	fwrite(&count, sizeof(int), 1, compressed);//���������� ���������� ��������
	fwrite(&ost, sizeof(int), 1, compressed);//�������� ������


	for (i = 0; i < 256; i++)	//���������� � ������ ���� ������� �������������
	{
		if (sym[i].freq > 0)
		{
			fwrite(&sym[i].ch, sizeof(unsigned char), 1, compressed);
			fwrite(&sym[i].freq, sizeof(float), 1, compressed);
		}
	}

	rewind(temp);//���������� ��������� � ������������� ����� � ������ �����

	j = 0;

	while ((arr[j] = fgetc(temp)) != EOF)
	{
		j++;

		if (j == 8)
		{
			code1.byte.b1 = arr[7] - '0';
			code1.byte.b2 = arr[6] - '0';
			code1.byte.b3 = arr[5] - '0';
			code1.byte.b4 = arr[4] - '0';
			code1.byte.b5 = arr[3] - '0';
			code1.byte.b6 = arr[2] - '0';
			code1.byte.b7 = arr[1] - '0';
			code1.byte.b8 = arr[0] - '0';
			fputc(code1.chh, compressed);

			for (i = 0; i < 8; i++)
				arr[i] = '0';

			j = 0;
		}
	}

	if (j > 0)
	{
		code1.byte.b1 = arr[7] - '0';
		code1.byte.b2 = arr[6] - '0';
		code1.byte.b3 = arr[5] - '0';
		code1.byte.b4 = arr[4] - '0';
		code1.byte.b5 = arr[3] - '0';
		code1.byte.b6 = arr[2] - '0';
		code1.byte.b7 = arr[1] - '0';
		code1.byte.b8 = arr[0] - '0';
		fputc(code1.chh, compressed);
	}



	fcloseall();
}

void decompressing() //�������, �������������� "����������" ������� �����
{
	struct SYM sym[256];
	struct SYM *psym[256];
	struct SYM *root;
	struct SYM *root_tmp;
	FILE * temp;
	FILE * compressed;
	FILE * decompressed;
	int count = 0;
	char str1[13] = "\0";
	char str2[13] = "compressing";
	int ost = 0;
	int i = 0;
	int raz;
	int tmp = 0;
	char file_name[30];
	int arr[8];//������ 0 � 1
	union code code1;//�������������� ���������� code1

	printf("Please, enter name of file for decompressing:\n");
	scanf("%s", &file_name);
	compressed = fopen(file_name, "rb");

	if (file_name == NULL)
	{
		perror("Can't open file!\n");
		exit(1);
	}

	fread(&str1, sizeof(char), 12, compressed); //��������,��� �� ��� ����,������� �� ����������.��� ����� ��������� ���������...


	if (strcmp(str1, str2) != 0) //...� ������� � ���,��� ������ ����
	return 0;
	
	fread(&count, sizeof(int), 1, compressed);//������ ���������� ���������� ��������
	fread(&ost, sizeof(int), 1, compressed);//������ �������� ������

	for (i = 0; i < count; i++)	//������ �� ������� ����� ������� �������������
	{

		fread(&sym[i].ch, sizeof(unsigned char), 1, compressed);
		fread(&sym[i].freq, sizeof(float), 1, compressed);

	}
	for (i = 0; i < 256; i++) //�������������� ������ �������� + ��������� � �������� ���������� �� ���������
	{
		psym[i] = &sym[i];
		sym[i].code[0] = '\0';
		sym[i].left = sym[i].right = 0;
	}

	sort_freq(psym); //��������� ������ ���������� �� ���������

	root = buildTree(psym, count); //������ ������ ��������

	makeCodes(root);

	temp = fopen("temp2.101", "wb");

	tmp = fgetc(compressed);

	while (tmp != EOF)
	{
		code1.chh = tmp;
		arr[7] = code1.byte.b1 + '0';
		arr[6] = code1.byte.b2 + '0';
		arr[5] = code1.byte.b3 + '0';
		arr[4] = code1.byte.b4 + '0';
		arr[3] = code1.byte.b5 + '0';
		arr[2] = code1.byte.b6 + '0';
		arr[1] = code1.byte.b7 + '0';
		arr[0] = code1.byte.b8 + '0';

		tmp = fgetc(compressed);

		if (tmp != EOF)
		{
			for (i = 0; i < 8; i++)
			{
				//putchar(arr[i]);
				fputc(arr[i], temp);
			}
		}
		else
		{
			for (i = 0; i < (ost == 0 ? 8 : ost); i++)// � ������, ���� ������� ����� 0, �� ���� ������ ����������� ��������� ����. ����� �������� �� ���������� ����� ������� 1 � 0, ������� ������� �������
			{
				//putchar(arr[i]);
				fputc(arr[i], temp);
			}
		}


	}
	tmp = 0;


	fclose(temp);

	temp = fopen("temp2.101", "rb");

	decompressed = fopen("decompressed.txt", "wt");

	rewind(temp);

	root_tmp = root;//����������,������� ����� ������� ������,����� ����� ���� � ���� ������������

	while ((tmp = fgetc(temp)) != EOF)
	{
		{
			if ((root->left == NULL) && (root->right == NULL))
			{
				fputc(root->ch, decompressed);
				root = root_tmp;
			}


			if (tmp == '0') //���� ��� � 0, ���� ������, ��� � 1, �� �������
				root = root->left;
			else
				root = root->right;

		}

	}
	if ((root->left == NULL) && (root->right == NULL))
	{
		fputc(root->ch, decompressed);
		root = root_tmp;
	}
	
	fcloseall();
}


int main()
{
	int menu;
	printf("Please select an action\nEnter:\n1 for compressing\n2 for decompressing\n");
	scanf("%d", &menu);

	if (menu == 1)
	{
		compressing();
		printf("file succesfully compressed\n");
	}
	else if (menu == 2)
	{
		decompressing();
		printf("file succesfully decompressed\n");
	}
	else
		printf("Input error!");
	return 0;
}