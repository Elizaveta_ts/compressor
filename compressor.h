#include <math.h>

struct SYM
{
	unsigned char ch; //ASCII-���
	float freq;//������� �������������
	char code[256];//������ ��� ������ ����
	struct  SYM *left ; //����� ������� � ������
	struct  SYM *right; //������ ������� � ������
};

union code
{
	unsigned char chh;//���������� ���������� ��� ��� ������ � ������ ����

	struct byte
	{
		unsigned b1 : 1;
		unsigned b2 : 1;
		unsigned b3 : 1;
		unsigned b4 : 1;
		unsigned b5 : 1;
		unsigned b6 : 1;
		unsigned b7 : 1;
		unsigned b8 : 1;
	} byte;
};

/*������� htree*/
struct SYM* buildTree(struct SYM *psym[], int N);

void makeCodes(struct SYM *root);

/*������� main*/

int find_count(struct SYM ** sym);

int print_freq(struct SYM * sym[]);

void sort_freq(struct SYM **psym);

void print_to_101(int ch, FILE *fp_in, FILE *fp_101, struct SYM * sym, int count);

void decompressing();

void compressing();