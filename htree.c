/*����, ���������� ������� ���������� � ������� ��������*/
#include <stdio.h>
#include <stdlib.h>
#include "compressor.h"

struct SYM* buildTree(struct SYM *psym[], int N)
{
	int i, j;
	//������� ��������� ����
	struct SYM *temp = (struct SYM*)malloc(sizeof(struct SYM));
	//� ���� ������� ������������ ����� ������
	//���������� � �������������� ��������� ������� psym
	temp->freq = psym[N - 2]->freq + psym[N - 1]->freq;
	//��������� ��������� ���� � ����� ���������� ������
	temp->left = psym[N - 1];
	temp->right = psym[N - 2];
	temp->code[0] = 0;
	if (N == 2) //�� ������������ �������� ������� � �������� 1.0
		return temp;
	//��������� temp � ������ ������� psym
	//�������� ������� �������� �������
	else //�������� � ������ � ������ ����� �������� ������ ��������
	{
		for (i = 0; i < N; i++)
			if (temp->freq > psym[i]->freq)
			{
				for (j = N - 1; j > i; j--)
					psym[j] = psym[j - 1];
				psym[i] = temp;
				break;
			}
	}
	//printf("%d", temp->freq);
	return buildTree(psym, N - 1);
}

void makeCodes(struct SYM *root) {
	

	if (root->left) {
		strcpy(root->left->code, root->code);
		strcat(root->left->code, "0");
		makeCodes(root->left);
	}
	if (root->right) {
		strcpy(root->right->code, root->code);
		strcat(root->right->code, "1");
		makeCodes(root->right);
	}
}


